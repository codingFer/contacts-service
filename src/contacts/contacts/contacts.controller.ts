import { Body, Controller, Get, Post } from '@nestjs/common';
import { ContactsService } from './contacts.service';
import { Contact } from './contact.entity';

@Controller('contacts')
export class ContactsController {
  constructor(private contactsService:ContactsService) {
  }

  @Get()
  getAll(): Promise<Contact[]> {
    return this.contactsService.findAll();
  }

  @Post('addContact')
  async create(@Body() payload: Contact): Promise<any> {
    return this.contactsService.create(payload);
  }
}
